<?php
require_once('config.php');
error_reporting(E_ALL & ~E_NOTICE);
$dpos = json_decode(file_get_contents('php://input'), true);
$kode = $dpos['kode'];

if ($kode == 'check_username') {
    $response = checkUsername($dpos['username']);
} else if ($kode == 'check_user_phone') {
    $response = checkUserPhone($dpos['no_hp']);
} else if ($kode == 'signup') {
    $response = signUp($dpos['patient_id'], $dpos['no_hp'], $dpos['username'], $dpos['password']);
} else if ($kode == 'login') {
    $response = login($dpos['username'], $dpos['password']);
} else if ($kode == 'change_pass') {
    $response = changePassword($dpos['username'], $dpos['password']);
} else if ($kode == 'check_patient') {
    $response = checkPatientData($dpos['nik'], $dpos['no_rm'], $dpos['no_bpjs']);
} else if ($kode == 'data_validation') {
    $response = dataValidation($dpos['no_hp'], $dpos['no_rm']);
} else {
    http_response_code(400);
    $response = array(
        'pesan' => 'Kode request tidak ditemukan!'
    );
}
header('Content-Type: application/json');
echo json_encode($response);

function checkUsername(?String $username)
{
    if (!is_null($username)) {
        global $db;

        $query = "SELECT users.id AS 'users_id', users.patient_id, users.no_hp AS 'user_phone', users.username, users.password, 
        patients.id AS 'patients_id', patients.nik, patients.no_rm, patients.no_bpjs, patients.nama, 
        patients.tanggal_lahir, patients.jenis_kelamin, 
        patients.no_hp AS 'patient_phone', patients.alamat_lengkap, patients.kabkota, patients.kecamatan, 
        patients.bahasa, patients.suku, patients.agama, patients.pendidikan, 
        patients.status_pernikahan, patients.nama_pasangan, patients.nama_ibu 
        FROM users JOIN patients ON users.patient_id = patients.id WHERE users.username = '$username'";

        $stmt = $db->prepare($query);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            http_response_code(200);
            return array(
                'id' => intval($result['users_id']),
                'patient_id' => is_null($result['patient_id']) ? $result['patient_id'] : intval($result['patient_id']),
                'no_hp' => $result['user_phone'],
                'username' => $result['username'],
                'patient' => is_null($result['patient_id']) ? $result['patient_id'] : (object) array(
                    'id' => intval($result['patients_id']),
                    'nik' => $result['nik'],
                    'no_rm' => $result['no_rm'],
                    'no_bpjs' => $result['no_bpjs'],
                    'nama' => $result['nama'],
                    'tanggal_lahir' => $result['tanggal_lahir'],
                    'jenis_kelamin' => $result['jenis_kelamin'],
                    'no_hp' => $result['patient_phone'],
                    'alamat_lengkap' => $result['alamat_lengkap'],
                    'kabkota' => $result['kabkota'],
                    'kecamatan' => $result['kecamatan'],
                    'bahasa' => $result['bahasa'],
                    'suku' => $result['suku'],
                    'agama' => $result['agama'],
                    'pendidikan' => $result['pendidikan'],
                    'status_pernikahan' => intval($result['status_pernikahan']),
                    'nama_pasangan' => $result['nama_pasangan'],
                    'nama_ibu' => $result['nama_ibu']
                ),
            );
        } else {
            $query = "SELECT users.id, users.patient_id, users.no_hp, users.username FROM users WHERE users.username = '$username'";

            $stmt = $db->prepare($query);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                http_response_code(200);
                return $result;
            } else {
                http_response_code(404);
                return array(
                    'pesan' => 'Username belum terdaftar!',
                );
            }
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang!'
        );
    }
}
function checkUserPhone(?String $no_hp)
{
    if (!is_null($no_hp)) {
        global $db;
        $query = "SELECT users.id AS 'users_id', users.patient_id, users.no_hp AS 'user_phone', users.username, users.password, 
        patients.id AS 'patients_id', patients.nik, patients.no_rm, patients.no_bpjs, patients.nama, 
        patients.tanggal_lahir, patients.jenis_kelamin, 
        patients.no_hp AS 'patient_phone', patients.alamat_lengkap, patients.kabkota, patients.kecamatan, 
        patients.bahasa, patients.suku, patients.agama, patients.pendidikan, 
        patients.status_pernikahan, patients.nama_pasangan, patients.nama_ibu 
        FROM users JOIN patients ON users.patient_id = patients.id WHERE users.no_hp = '$no_hp'";

        $stmt = $db->prepare($query);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            http_response_code(200);
            return array(
                'id' => intval($result['users_id']),
                'patient_id' => is_null($result['patient_id']) ? $result['patient_id'] : intval($result['patient_id']),
                'no_hp' => $result['user_phone'],
                'username' => $result['username'],
                'patient' => is_null($result['patient_id']) ? $result['patient_id'] : (object) array(
                    'id' => intval($result['patients_id']),
                    'nik' => $result['nik'],
                    'no_rm' => $result['no_rm'],
                    'no_bpjs' => $result['no_bpjs'],
                    'nama' => $result['nama'],
                    'tanggal_lahir' => $result['tanggal_lahir'],
                    'jenis_kelamin' => $result['jenis_kelamin'],
                    'no_hp' => $result['patient_phone'],
                    'alamat_lengkap' => $result['alamat_lengkap'],
                    'kabkota' => $result['kabkota'],
                    'kecamatan' => $result['kecamatan'],
                    'bahasa' => $result['bahasa'],
                    'suku' => $result['suku'],
                    'agama' => $result['agama'],
                    'pendidikan' => $result['pendidikan'],
                    'status_pernikahan' => intval($result['status_pernikahan']),
                    'nama_pasangan' => $result['nama_pasangan'],
                    'nama_ibu' => $result['nama_ibu']
                ),
            );
        } else {
            $query = "SELECT users.id, users.patient_id, users.no_hp, users.username FROM users WHERE users.no_hp = '$no_hp'";

            $stmt = $db->prepare($query);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                http_response_code(200);
                return $result;
            } else {
                http_response_code(404);
                return array(
                    'pesan' => 'Nomor HP belum terdaftar!',
                );
            }
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang!'
        );
    }
}

function dataValidation(?String $no_hp, ?String $no_rm)
{

    $is_request_null = is_null($no_rm) || is_null($no_hp);
    $is_request_empty = $no_rm == "" || $no_hp == "";

    if (!$is_request_null && !$is_request_empty) {
        global $db;
        $query = "SELECT * FROM patients WHERE patients.no_hp = '$no_hp' AND patients.no_rm = '$no_rm'";

        $stmt = $db->prepare($query);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        } else {
            http_response_code(404);
            return array(
                'pesan' => 'Tidak ditemukan data yang sesuai!'
            );
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang atau kosong!'
        );
    }
}

function signUp(?String $patient_id, ?String $no_hp, ?String $username, ?String $password)
{
    $is_required_request_null = is_null($username) || is_null($password) || is_null($no_hp);
    $is_required_request_empty = $username == "" || $password == "" || $no_hp == "";
    if (!$is_required_request_null && !$is_required_request_empty) {
        global $db;
        $registered_phone = checkUserPhone($no_hp);
        $registered_usernames = checkUsername($username);
        if ($username == $registered_usernames['username']) {
            http_response_code(403);
            return array(
                'pesan' => 'Username sudah terdaftar!'
            );
        }
        if (($no_hp == $registered_phone['user_phone']) || ($no_hp == $registered_phone['no_hp'])) {
            http_response_code(403);
            return array(
                'pesan' => 'No HP sudah terdaftar!'
            );
        }
        global $db;
        $pass_hash = password_hash($password, PASSWORD_DEFAULT);

        if (is_null($patient_id) || $patient_id == "") {
            $query = "INSERT INTO `users` (`username`, `password`, `no_hp`) VALUES ('$username', '$pass_hash', '$no_hp');";
        } else {
            $query = "INSERT INTO `users` (`patient_id`, `username`, `password`, `no_hp`) VALUES ('$patient_id', '$username', '$pass_hash', '$no_hp');";
        }

        $stmt = $db->prepare($query);

        $stmt->execute();

        if ($stmt) {
            $result = checkUsername($username);
            if (http_response_code() == 200) {
                return $result;
            } else {
                http_response_code(500);
                return array(
                    'pesan' => 'Gagal mendapatkan data user yang didaftarkan! Mohon maaf ada kesalahan pada server kami.',
                );
            }
        } else {
            http_response_code(500);
            return array(
                'pesan' => 'Gagal mendaftar! Ada kesalahan pada server kami.',
            );
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang atau kosong!'
        );
    }
}

function login(?String $username, ?String $password)
{
    $is_request_null = is_null($username) || is_null($password);
    $is_request_empty = $username == "" || $password == "";
    if (!$is_request_null && !$is_request_empty) {
        global $db;
        $query = "SELECT users.id AS 'users_id', users.patient_id, users.no_hp AS 'user_phone', users.username, users.password, 
        patients.id AS 'patients_id', patients.nik, patients.no_rm, patients.no_bpjs, patients.nama, 
        patients.tanggal_lahir, patients.jenis_kelamin, 
        patients.no_hp AS 'patient_phone', patients.alamat_lengkap, patients.kabkota, patients.kecamatan, 
        patients.bahasa, patients.suku, patients.agama, patients.pendidikan, 
        patients.status_pernikahan, patients.nama_pasangan, patients.nama_ibu 
        FROM users JOIN patients ON users.patient_id = patients.id WHERE users.username = '$username'";

        $stmt = $db->prepare($query);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            if (password_verify($password, $result['password'])) {
                return array(
                    'id' => intval($result['users_id']),
                    'patient_id' => is_null($result['patient_id']) ? $result['patient_id'] : intval($result['patient_id']),
                    'no_hp' => $result['user_phone'],
                    'username' => $result['username'],
                    'patient' => is_null($result['patient_id']) ? $result['patient_id'] : (object) array(
                        'id' => intval($result['patients_id']),
                        'nik' => $result['nik'],
                        'no_rm' => $result['no_rm'],
                        'no_bpjs' => $result['no_bpjs'],
                        'nama' => $result['nama'],
                        'tanggal_lahir' => $result['tanggal_lahir'],
                        'jenis_kelamin' => $result['jenis_kelamin'],
                        'no_hp' => $result['patient_phone'],
                        'alamat_lengkap' => $result['alamat_lengkap'],
                        'kabkota' => $result['kabkota'],
                        'kecamatan' => $result['kecamatan'],
                        'bahasa' => $result['bahasa'],
                        'suku' => $result['suku'],
                        'agama' => $result['agama'],
                        'pendidikan' => $result['pendidikan'],
                        'status_pernikahan' => intval($result['status_pernikahan']),
                        'nama_pasangan' => $result['nama_pasangan'],
                        'nama_ibu' => $result['nama_ibu']
                    ),
                );
            } else {
                echo "Panjang ";
                http_response_code(401);
                return array(
                    'pesan' => 'Username atau Password salah!',
                );
            }
        } else {
            $query = "SELECT * FROM users WHERE users.username = '$username'";

            $stmt = $db->prepare($query);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                if (password_verify($password, $result['password'])) {
                    return array(
                        'id' => intval($result['id']),
                        'patient_id' => is_null($result['patient_id']) ? $result['patient_id'] : intval($result['patient_id']),
                        'username' => $result['username'],
                    );
                } else {
                    http_response_code(401);
                    return array(
                        'pesan' => 'Username atau Password salah!',
                    );
                }
            }
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang atau kosong!'
        );
    }
}

function changePassword(?String $username, ?String $password)
{
    $is_request_null = is_null($username) || is_null($password);
    $is_request_empty = $username == "" || $password == "";

    if (!$is_request_empty && !$is_request_null) {
        global $db;
        $registered_usernames = checkUsername($username);
        if ($registered_usernames['username'] == $username) {
            $pass_hash = password_hash($password, PASSWORD_DEFAULT);
            $query = "UPDATE users SET password = $pass_hash WHERE users.username = $username";

            $stmt = $db->prepare($query);

            $stmt->execute();

            if ($stmt) {
                http_response_code(200);
                return array(
                    'pesan' => 'Password berhasil diubah!'
                );
            } else {
                http_response_code(500);
                return array(
                    'pesan' => 'Ada kesalahan!'
                );
            }
        } else {
            http_response_code(404);
            return array(
                'pesan' => 'Username tidak ditemukan!'
            );
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang atau kosong!'
        );
    }
}

function checkPatientData(?String $nik, ?String $no_rm, ?String $no_bpjs)
{
    $all_request_null = is_null($no_rm) && is_null($no_bpjs) && is_null($nik);
    $all_request_empty = $no_rm == "" && $no_bpjs == "" && $nik == "";

    if (!$all_request_empty && !$all_request_null) {
        global $db;
        $query = "SELECT * FROM patients WHERE patients.no_rm = '$no_rm' OR patients.no_bpjs = '$no_bpjs' OR patients.nik = '$nik'";

        $stmt = $db->prepare($query);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        } else {
            http_response_code(404);
            return array(
                'pesan' => 'Data Pasien tidak ditemukan!'
            );
        }
    } else {
        http_response_code(400);
        return array(
            'pesan' => 'Parameter ada yang kurang atau kosong!'
        );
    }
}
